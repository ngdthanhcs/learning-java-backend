package com.example.demo.router;

import com.example.demo.app.DogAppService;
import com.example.demo.domain.Dog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    DogAppService DogAppService = new DogAppService();

    @RequestMapping("/breed")
    public Dog[] Breed(Dog father, Dog mother)
    {
        return DogAppService.breedOffspring(father, mother);
    }

    @RequestMapping("/getall")
    public Dog GetAll(){
        return DogAppService.GetAll();
    }
}
