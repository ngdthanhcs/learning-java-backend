package com.example.demo.app;

import com.example.demo.domain.Dog;
import org.yaml.snakeyaml.util.ArrayUtils;

import java.util.List;
import java.util.Random;

public class DogAppService {
    Random random = new Random();

    public Dog[] breedOffspring(Dog father, Dog mother, String name) {
        var offspringNumber = random.nextInt(12);
        if (offspringNumber == 0) {
            return null;
        }

        var offspringArray = new Dog[offspringNumber];

        for (int i = 0; i < offspringNumber; i++) {
            offspringArray[i] = new Dog(
                    father.breed,
                    name,
                    random.nextDouble() > 0.6
                            ? mother.hairColor
                            : father.hairColor,
                    0,
                    father.gen >= mother.gen
                            ? father.gen++
                            : mother.gen++

            );
        }

        return offspringArray;
    }

    public Dog GetAll() {

    }
}
